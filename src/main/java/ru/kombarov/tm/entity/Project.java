package ru.kombarov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

import static ru.kombarov.tm.util.DateUtil.parseDateToString;

public final class Project extends AbstractEntity {

    @Getter
    @Setter
    @Nullable
    private String name;

    @Getter
    @Setter
    @Nullable
    private String userId;

    @Getter
    @Setter
    @Nullable
    private String description;

    @Setter
    @Nullable
    private Date dateStart;

    @Setter
    @Nullable
    private Date dateFinish;

    public Project(final @Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getDateStart() {
        if (dateStart == null) return null;
        return parseDateToString(dateStart);
    }

    @Nullable
    public String getDateFinish() {
        if (dateFinish == null) return null;
        return parseDateToString(dateFinish);
    }
}
