package ru.kombarov.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum Role {

    ADMINISTRATOR("Administrator"),
    USER("User");

    final private @NotNull String roleName;
}
