package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Project;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;
import static ru.kombarov.tm.util.EntityUtil.printProjects;

public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT EDIT]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        printProjects(serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("ENTER PROJECT NAME FOR EDIT");
        final @Nullable String nameAnotherProject = input.readLine();
        final @NotNull Project anotherProject = new Project(nameAnotherProject);
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherProject.setDateStart(parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        anotherProject.setDateFinish(parseStringToDate(input.readLine()));
        anotherProject.setId(serviceLocator.getProjectService().getIdByName(nameAnotherProject));
        anotherProject.setUserId(serviceLocator.getUserService().getUserCurrent().getId());
        serviceLocator.getProjectService().merge(anotherProject);
        System.out.println("[OK]");
    }
}
