package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Task;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK EDIT]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        printTasks(serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("ENTER TASK NAME FOR EDIT");
        final @Nullable String nameAnotherTask = input.readLine();
        final @NotNull Task anotherTask = new Task(nameAnotherTask);
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherTask.setDateStart(parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        anotherTask.setDateFinish(parseStringToDate(input.readLine()));
        anotherTask.setId(serviceLocator.getTaskService().getIdByName(nameAnotherTask));
        anotherTask.setUserId(serviceLocator.getUserService().getUserCurrent().getId());
        serviceLocator.getTaskService().merge(anotherTask);
        System.out.println("[OK]");
    }
}
