package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Task;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME");
        final @NotNull Task task = new Task(input.readLine());
        System.out.println("ENTER TASK DESCRIPTION");
        task.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        task.setDateStart(parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        task.setDateFinish(parseStringToDate(input.readLine()));
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        task.setUserId(serviceLocator.getUserService().getUserCurrent().getId());
        serviceLocator.getTaskService().persist(task);
        System.out.println("[OK]");
    }
}
