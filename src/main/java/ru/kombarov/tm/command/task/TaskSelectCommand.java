package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printTask;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskSelectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-select";
    }

    @NotNull
    @Override
    public String description() {
        return "Select the task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SELECT]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        printTasks(serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("ENTER TASK NAME");
        printTask(serviceLocator.getTaskService().findOne(serviceLocator.getTaskService().getIdByName(input.readLine())));
        System.out.println("[OK]");
    }
}
