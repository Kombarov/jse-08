package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.User;

import static ru.kombarov.tm.util.PasswordUtil.generateHash;

public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN");
        @Nullable String login = input.readLine();
        if (serviceLocator == null) return;
        while (!serviceLocator.getUserService().checkLogin(login)) {
            System.out.println("LOGIN DOES NOT EXIST");
            System.out.println("ENTER LOGIN");
            login = input.readLine();
        }
        System.out.println("[OK]");
        @Nullable User user = null;
        for (@NotNull User user1 : serviceLocator.getUserService().findAll()) {
            if (user1.getLogin() == null) return;
            if (user1.getLogin().equals(login)) user = user1;
        }
        System.out.println("ENTER PASSWORD");
        @Nullable String password = input.readLine();
        if (user == null || user.getPassword() == null) return;
        while (!user.getPassword().equals(generateHash(password))) {
            System.out.println("INVALID PASSWORD");
            System.out.println("ENTER PASSWORD");
            password = input.readLine();
        }
        serviceLocator.getUserService().setUserCurrent(user);
        System.out.println("[OK]");
    }
}
