package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.User;

public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit user.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER EDIT]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        System.out.println("YOUR USERNAME IS " + serviceLocator.getUserService().getUserCurrent().getLogin());
        System.out.println("ENTER NEW USERNAME");
        final @Nullable String login = input.readLine();
        @NotNull User user = new User();
        user = serviceLocator.getUserService().getUserCurrent();
        user.setLogin(login);
        serviceLocator.getUserService().setUserCurrent(user);
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }
}
