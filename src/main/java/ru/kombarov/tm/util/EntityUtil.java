package ru.kombarov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.entity.User;

import java.util.List;

public final class EntityUtil {

    public static void printTask(final @Nullable Task task) throws Exception {
        if (task == null) throw new Exception();
        System.out.println("task name: " + task.getName());
        System.out.println("task description: " + task.getDescription());
        System.out.println("start date: " + task.getDateStart());
        System.out.println("end date: " + task.getDateFinish());
    }

    public static void printTasks(final @NotNull List<Task> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println(i+1 + ". " + tasks.get(i).getName());
        }
    }

    public static void printProject(final @Nullable Project project) throws Exception {
        if (project == null) throw new Exception();
        System.out.println("project name: " + project.getName());
        System.out.println("project description: " + project.getDescription());
        System.out.println("start date: " + project.getDateStart());
        System.out.println("end date: " + project.getDateFinish());
    }

    public static void printProjects(final @NotNull List<Project> projects) {
        for (int i = 0; i < projects.size(); i++) {
            System.out.println(i+1 + ". " + projects.get(i).getName());
        }
    }

    public static void printUser(final @Nullable User user) throws Exception {
        if (user == null || user.getRole() == null) throw new Exception();
        System.out.println("username: " + user.getLogin());
        System.out.println("role: " + user.getRole().getRoleName() + "\n");
        System.out.println("projects:");
    }

    public static void printUsers(final @NotNull List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            System.out.println(i+1 + ". " + users.get(i).getLogin());
        }
    }
}
