package ru.kombarov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {

    @NotNull
    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static Date parseStringToDate(final @NotNull String stringDate) {
        return dateFormatter.parse(stringDate, new ParsePosition(0));
    }

    @NotNull
    public static String parseDateToString(final @NotNull Date date) {
        return dateFormatter.format(date);
    }
}
