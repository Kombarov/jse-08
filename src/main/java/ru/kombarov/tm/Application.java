package ru.kombarov.tm;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.project.*;
import ru.kombarov.tm.command.system.*;
import ru.kombarov.tm.command.task.*;
import ru.kombarov.tm.command.user.*;
import ru.kombarov.tm.context.Bootstrap;

public final class Application {

    @NotNull
    private static final Class[] CLASSES = {
            ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectEditCommand.class, ProjectListCommand.class,
            ProjectRemoveCommand.class, ProjectSelectCommand.class,
            AboutCommand.class, HelpCommand.class,
            TaskAttachCommand.class, TaskClearCommand.class,
            TaskCreateCommand.class, TaskEditCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class,
            TaskSelectCommand.class, TaskShowByProjectCommand.class,
            TaskUnattachCommand.class, UserChangePasswordCommand.class,
            UserCreateCommand.class, UserEditCommand.class,
            UserLoginCommand.class, UserLogoutCommand.class,
            UserRemoveCommand.class, UserViewCommand.class
    };

    public static void main(String[] args) throws Exception {

        final @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }
}
