package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.IService;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.repository.AbstractRepository;
import ru.kombarov.tm.repository.UserRepository;

import java.util.List;

public final class UserService extends AbstractService<User> implements IService<User> {

    @Nullable
    private User userCurrent;

    @NotNull
    private final UserRepository projectRepository = (UserRepository) abstractRepository;

    public UserService(AbstractRepository<User> abstractRepository) {
        super(abstractRepository);
    }

    @Nullable
    @Override
    public String getIdByName(final @Nullable String name) throws Exception {
        if (name == null || name.isEmpty()) return null;
        else return abstractRepository.getIdByName(name);
    }

    @NotNull
    @Override
    public List<User> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return abstractRepository.findAll(userId);
    }

    @Nullable
    @Override
    public User findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        else return abstractRepository.findOne(userId, id);
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        abstractRepository.removeAll(userId);
    }

    public void setUserCurrent(final @Nullable User user) {
        userCurrent = user;
    }

    @Nullable
    public User getUserCurrent() {
        return userCurrent;
    }

    public boolean checkLogin(final @Nullable String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        return projectRepository.checkLogin(login);
    }
}