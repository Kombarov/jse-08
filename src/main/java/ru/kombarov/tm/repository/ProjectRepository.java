package ru.kombarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IRepository<Project> {

    @Override
    public void removeAll() {
        entityMap.clear();
    }

    @NotNull
    @Override
    public List<Project> findAll(final @NotNull String userId) throws Exception {
        final @NotNull List<Project> list = new ArrayList<Project>();
        for (final @NotNull Project project : findAll()) {
            if (project.getUserId() == null) throw new Exception();
            if (project.getUserId().equals(userId)) list.add(project);
        }
        return list;
    }

    @Nullable
    @Override
    public Project findOne(final @NotNull String userId, final @NotNull String id) throws Exception {
        for (final @NotNull Project project : findAll(userId)) {
            if (project.getId() == null) throw new Exception();
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    @Override
    public void removeAll(final @NotNull String userId) throws Exception {
        for (final @NotNull Project project : findAll()) {
            if (project.getUserId() == null || project.getId() == null) throw new Exception();
            if (project.getUserId().equals(userId)) remove(project.getId());
        }
    }

    @Nullable
    @Override
    public String getIdByName(final @NotNull String name) {
        final @NotNull List<Project> projects = findAll();
        for (final @NotNull Project project : projects) {
            if (project.getName() == null) return null;
            if (project.getName().equals(name)) return project.getId();
        }
        return null;
    }

    @NotNull
    public List<Project> getProjectsByUserId(final @NotNull String userId) throws Exception {
        final @NotNull List<Project> list = findAll();
        final @NotNull List<Project> sortedList = new ArrayList<>();
        for (final @NotNull Project project : list) {
            if (project.getUserId() == null) throw new Exception();
            if (project.getUserId().equals(userId)) sortedList.add(project);
        }
        return sortedList;
    }
}
