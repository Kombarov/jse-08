package ru.kombarov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @NotNull
    List<T> findAll();

    @NotNull
    List<T> findAll(@Nullable String userId) throws Exception;

    @Nullable
    T findOne(@Nullable String id);

    @Nullable
    T findOne(@Nullable String userId,@Nullable String id) throws Exception;

    void persist(@Nullable T t) throws Exception;

    void merge(@Nullable T t) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void removeAll();

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    String getIdByName(@Nullable String name) throws Exception;
}
