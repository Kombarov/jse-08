package ru.kombarov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {

    @NotNull
    List<T> findAll();

    @NotNull
    List<T> findAll(@NotNull String userId) throws Exception;

    @Nullable
    T findOne(@NotNull String id);

    @Nullable
    T findOne(@NotNull String userId,@NotNull String id) throws Exception;

    void persist(@NotNull T t) throws Exception;

    void merge(@NotNull T t) throws Exception;

    void remove(@NotNull String id) throws Exception;

    void removeAll();

    void removeAll(@NotNull String userId) throws Exception;

    @Nullable
    String getIdByName(@NotNull String name) throws Exception;
}
